function renderTable() {
     var tableObj = $("#tableRender");
     tableObj.empty();
     var responseData = getData();
     var builtData = parseData(responseData);
     var renderedCardList = renderCardList(builtData);
     tableObj.append(renderedCardList);
}
function getData() {
     var apiSettings = {
          url: "https://randomuser.me/api/?results=3",
          async: false,
     };
     var response = {};
     $.ajax(apiSettings).done(function(data) {
          response = data.results;
     });
     return response;
}
function parseData(dataList) {
     var fieldsToExtract = [
         "name",
         "picture",
         "location",
         "phone"
     ];
     var parsedData = [];
     dataList.forEach(function (item) {
          var tempItem = [];
          fieldsToExtract.forEach(function (field) {
               tempItem[field] = item[field];
          });
          parsedData.push(tempItem);
     });
     return parsedData;
}
function renderCardList(parsedDataList) {
    // determine mobile or desktop based on screen size
     var uiType = isMobile() ? 'mobileUI' : 'desktopUI';
     var card = `<ul class='${uiType}'>`;
     parsedDataList.forEach(function (item) {
          card += `<li class="singleCard"><div>
                            <table>
                            <tr>
                                <td>
                                    <img src="${item.picture.thumbnail}" />
                                </td>
                                <td class="textTd">
                                     ${item.name.title} ${item.name.first} ${item.name.last} <br>
                                     ${item.location.country} <br>
                                     ${item.location.city} <br>
                                     ${item.phone}
                                </td>
                            </tr>
                            </table>
                   </div></li>`;
     });
     card += "</ul>";
     return card;
}
function isMobile() {
     var screenWidth = $(window).width();
     return screenWidth < 700;
}